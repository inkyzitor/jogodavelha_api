﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace jogoVelha.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class JogoController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<JogoController> _logger;

        public JogoController(ILogger<JogoController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<ApiJogoVelha> Get()
        {
            return;
        }
    }
}
