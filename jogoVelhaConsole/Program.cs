﻿using System;
using jogoDaVelha;

namespace jogoVelhaConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //Tabuleiro tabuleiro = new Tabuleiro();
            //jogodaVelha tudo = new jogodaVelha();
            IjogodaVelha tudo = jogodaVelha.Criar();
            Console.WriteLine("opcoes para jogadores: X ou O");
            Console.WriteLine("Digite seu simbolo Jogador 1: ");
            char icone1 = Convert.ToChar(Console.ReadLine());
            Jogador player1 = new Jogador(icone1);

            Console.WriteLine("Digite seu simbolo Jogador 2: ");
            char icone2 = Convert.ToChar(Console.ReadLine());
            Jogador player2 = new Jogador(icone2);

            bool finalizar = false;
            int jogadas = 0;

            do
            {
                Console.WriteLine(tudo.ImprimirTabuleiro());
                Console.WriteLine("Jogador 1: ");
                tudo.Jogar(Convert.ToInt32(Console.ReadLine()), Convert.ToInt32(Console.ReadLine()), player1);
                jogadas++;

                Console.WriteLine(tudo.ImprimirTabuleiro());
                if(finalizar){Console.WriteLine("Jogador 1 foi o vencedor!");}
                if(tudo.Empatou()){finalizar = true; Console.WriteLine("Empate");};

                if(!finalizar)
                {
                    Console.WriteLine("Jogador 2: ");
                    tudo.Jogar(Convert.ToInt32(Console.ReadLine()), Convert.ToInt32(Console.ReadLine()), player2);
                    jogadas++;

                    Console.WriteLine(tudo.ImprimirTabuleiro());
                    if(finalizar){Console.WriteLine("Jogador 2 foi o vencedor!");}
                    if(tudo.Empatou()){finalizar = true; Console.WriteLine("Empate");};
                }


            } while (!finalizar);
            Console.WriteLine("fim de jogo!");
        }
    }
}
