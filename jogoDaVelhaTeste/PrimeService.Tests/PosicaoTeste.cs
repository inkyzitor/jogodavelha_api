using System;
using Xunit;
using jogoDaVelha;

namespace PrimeService.Tests
{
    public class PosicaoTeste
    {
        [Fact]
        public void TestPosicaoValida()
        {
            jogoDaVelha.Tabuleiro tabuleiro = new jogoDaVelha.Tabuleiro();
            tabuleiro.guardaPosicao(0,0, 'x');
            Assert.Throws<ArgumentException>(()=> tabuleiro.guardaPosicao(0,0, 'x'));
        }
        [Fact]
        public void TestPosicaoInvalida()
        {
            jogoDaVelha.Tabuleiro tabuleiro = new jogoDaVelha.Tabuleiro();
            tabuleiro.guardaPosicao(0,0, 'o');
            Assert.Throws<ArgumentException>(()=> tabuleiro.guardaPosicao(0,0, 'x'));         
        }

    }
}