using System;
using Xunit;
using jogoDaVelha;

namespace PrimeService.Tests
{
    public class TabuleiroTeste
    {
        
        [Fact]
        public void TesteVitotriaValida()
        {
            jogoDaVelha.Tabuleiro tabuleiro = new jogoDaVelha.Tabuleiro();
            tabuleiro.guardaPosicao(0,0, 'x');
            tabuleiro.guardaPosicao(0,1, 'x');
            tabuleiro.guardaPosicao(0,2, 'x');

            Assert.Throws<ArgumentException>(()=> tabuleiro.guardaPosicao(0,0, 'x'));
            Assert.Throws<ArgumentException>(()=> tabuleiro.guardaPosicao(0,1, 'x'));
            Assert.Throws<ArgumentException>(()=> tabuleiro.guardaPosicao(0,2, 'x'));
           
        }
        [Fact]
        public void TestarSePosicaoTemTamanhoInvalido()
        {
            jogoDaVelha.Tabuleiro tabuleiro = new jogoDaVelha.Tabuleiro();
            Assert.Throws<IndexOutOfRangeException>(()=> tabuleiro.guardaPosicao(4,4, 'x'));
        
        }

        [Fact]
        public void TestarSeGanhouHorizontal()
        {
            Jogador jogador = new Jogador('X');
            jogoDaVelha.Tabuleiro tabuleiro = new jogoDaVelha.Tabuleiro();
            jogador.Jogar(0,0, tabuleiro);
            jogador.Jogar(0,1, tabuleiro);
            jogador.Jogar(0,2, tabuleiro);

            Assert.True(tabuleiro.ValidarVitotria());
        }
         [Fact]
        public void TestarSeGanhouVertical()
        {
            Jogador jogador = new Jogador('X');
            jogoDaVelha.Tabuleiro tabuleiro = new jogoDaVelha.Tabuleiro();
            jogador.Jogar(0,0, tabuleiro);
            jogador.Jogar(1,0, tabuleiro);
            jogador.Jogar(2,0, tabuleiro);

            Assert.True(tabuleiro.ValidarVitotria());
        }

         [Fact]
        public void TestarSeGanhouDiagonal()
        {
            Jogador jogador = new Jogador('X');
            jogoDaVelha.Tabuleiro tabuleiro = new jogoDaVelha.Tabuleiro();
            jogador.Jogar(0,0, tabuleiro);
            jogador.Jogar(1,1, tabuleiro);
            jogador.Jogar(2,2, tabuleiro);

            Assert.True(tabuleiro.ValidarVitotria());
        }

       

  
    }
}
