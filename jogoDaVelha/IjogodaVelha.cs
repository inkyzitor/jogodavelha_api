namespace jogoDaVelha
{
    public interface IjogodaVelha
    {

        public void Jogar(int x,int y, Jogador jogador);
        
        public bool VerificarVencedor(Jogador jogador);
        
        public bool Empatou();
        
        public string ImprimirTabuleiro();
       
    }
}