namespace jogoDaVelha
{
    public class Jogador
    {
        //+jogar(posicao, X, O)e ttabuleiro.
       private char simbolo;
       public Jogador(char simbolo)
       {
           this.simbolo = simbolo;
       }

       public void Jogar(int x, int y, Tabuleiro tabuleiro)
       {
           new Posicao(x,y, simbolo).MarcaTabuleiro(tabuleiro);
       } 

       public bool Ganhou(Tabuleiro tabuleiro)
       {
           return tabuleiro.ValidarVitotria();
       }

    }
}