using System;
using System.Collections.Generic;

namespace jogoDaVelha
{
    public class Tabuleiro
    {

        //+armazenar posicao(posicao)
        //-validar posicao
        //+verificar ganhador()

        private char[,] posicaoMatriz = new char[3, 3];

        // List<string> escolhasNumeros = new List<string> {};
        // int escolhas = 1;                                        } ideia anterior
        // int tentativas = 1;

        private int turnos = 0;

        public void guardaPosicao(int x, int y, char simbolo)
        {
            espacoValido(x,y);
            posicaoMatriz[x,y] = simbolo;
            turnos = turnos + 1;
            
        }

        private void espacoValido(int x, int y)
        {
            if(!PosicaoDisponivel(x,y)){
                throw new ArgumentException("Posição não esta disponivel!");
            }
            
            if(!ValoresValidos(x,y))
            {
                throw new IndexOutOfRangeException("Posição não é válida!");
            }
        }

        private bool ValoresValidos(int x, int y)
        {
            x = 0; 
            y = 0;

            if(posicaoMatriz.GetLength(x) < 3 || posicaoMatriz.GetLength(x) < 0 || posicaoMatriz.GetLength(y) < 3 || posicaoMatriz.GetLength(y) < 0)
            {
                return false;
            }

            return true;
        }

        private bool PosicaoDisponivel(int x, int y)
        {
            if(posicaoMatriz[x,y] != 0)
            {
                return false;
            }

            return true;
        }

        //possibilidades do jogo...

        public bool Empatou()
        {
            if(turnos == 9 && !ValidarVitotria())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ValidarVitotria()
        {
            for (int i = 0; i < 3; i++)
            {
                if(posicaoMatriz[i,0] != 0 && posicaoMatriz[i,0] == posicaoMatriz[i,1] && posicaoMatriz[i,0] == posicaoMatriz[i,2])
                {
                    return true;
                }
                if(posicaoMatriz[0,i] != 0 && posicaoMatriz[0,i] == posicaoMatriz[1,i] && posicaoMatriz[0,i] == posicaoMatriz[2,i])
                {
                    return true;
                }if(posicaoMatriz[i*2,0] != 0 && posicaoMatriz[i*2,0] == posicaoMatriz[1,1] && posicaoMatriz[i*2,0] == posicaoMatriz[2-i*2,2])
                {
                    return true;
                }  
            }
            return false;
        }

        

        public string ImprimirTabuleiro()
        {
            return  posicaoMatriz[0,0].ToString() + " | " + posicaoMatriz[0,1].ToString() + " " + posicaoMatriz[0,2].ToString() + "\n" +
                    "---|---|---\n" +
                    posicaoMatriz[1,0].ToString() + " | " + posicaoMatriz[1,1].ToString() + " " + posicaoMatriz[1,2].ToString() + "\n" +
                    "---|---|---\n" +
                    posicaoMatriz[2,0].ToString() + " | " + posicaoMatriz[2,1].ToString() + " " + posicaoMatriz[2,2].ToString() + "\n";
 
        }


   
    }
}