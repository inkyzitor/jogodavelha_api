using System.Reflection.Metadata.Ecma335;
namespace jogoDaVelha
{
    public class jogodaVelha : IjogodaVelha
    {
        private Tabuleiro tabuleiro = new Tabuleiro();

        public void Jogar(int x,int y, Jogador jogador)
        {
            jogador.Jogar(x, y, tabuleiro);
        }
        public bool VerificarVencedor(Jogador jogador)
        {
            return jogador.Ganhou(tabuleiro);

        }
        public bool Empatou()
        {
            return tabuleiro.Empatou();
        }
        public string ImprimirTabuleiro()
        {
            return tabuleiro.ImprimirTabuleiro();
        }
        public static IjogodaVelha Criar()
        {
            return new jogodaVelha();
        }
    }
}