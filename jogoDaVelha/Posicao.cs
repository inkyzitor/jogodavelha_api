﻿using System;

namespace jogoDaVelha
{
    public class Posicao
    {

        //+marcar no tabluleiro(tabuleiro)

        private int x;
        private int y;  
        private char simbolo;

        public Posicao(int x, int y, char simbolo)
        {
            this.x = x;
            this.y = y;
            this.simbolo = simbolo;
        }   
        public void MarcaTabuleiro(Tabuleiro tabuleiro)
        {
           tabuleiro.guardaPosicao(x,y, simbolo);
        }
        


    }
}
